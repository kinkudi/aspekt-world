import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import processing.core.PVector;


public class Graph {
	public class Types {
		public static final int RANDOM_GRAPH				 = 0;
		public static final int RANDOM_EMPTY_GRAPH			 = 1;
		public static final int LAUNCH_ANIMATION_GRAPH		 = 2;
		public static final int RANDOM_TREE					 = 3;
		public static final int TYPE_FIELD_COUNT			 = 4;
	}
	public int animeFrame = 3;
	public final AspektWorld aw;
	public final int ecMask = (255 << 24);
	public final List<Vector> vectors;
	public final List<Edge> edges;
	public final Map<Vector,Vector> vm;
	public final Map<Edge,Edge> em;
	public final Map<Vector,PVector> posm;
	public volatile long vid, eid;
	public final int type;
	public final int randomVertices = 2800;
	public volatile Vector sourceField;
	public HashSet<Vector> fielded;
	public ArrayList<Vector> frontier, nextFrontier, temp;
	public HashMap<Vector,HashMap<Vector,Integer>> distances;
	public int count;
	public int ecolor;

	public Graph( AspektWorld awa, int gtype ) {
		aw = awa;
		vectors = new ArrayList<Vector>();
		edges = new ArrayList<Edge>();
		vm = new HashMap<Vector,Vector>();
		em = new HashMap<Edge,Edge>();
		posm = new HashMap<Vector,PVector>();
		type = gtype;
		switch(type) {
		case(Types.RANDOM_GRAPH):
			randomGraph();
		break;
		case(Types.LAUNCH_ANIMATION_GRAPH):
			randomEmptyGraph();
		makeEdgeFieldSource();
		break;
		case(Types.RANDOM_EMPTY_GRAPH):
			randomEmptyGraph();
		break;
		default:
			break;
		}
	}

	public void makeEdgeFieldSource() {
		sourceField = vectors.get((int)aw.random(vectors.size()));
		setPos(sourceField, new PVector(0,-110,0));
		fielded = new HashSet<Vector>(vectors.size());
		frontier = new ArrayList<Vector>(vectors.size()/2);
		nextFrontier = new ArrayList<Vector>(vectors.size()/2);
		frontier.add(sourceField);
		distances = new HashMap<Vector,HashMap<Vector,Integer>>(vectors.size());
		makeDistances();
		update_edge_field();
	}

	public void makeDistances() {
		int i = vectors.size();
		while(--i >= 0) {
			Vector v = vectors.get(i);
			int j = vectors.size();
			HashMap<Vector,Integer> vds = new HashMap<Vector,Integer> ();
			while(--j >= 0) {
				Vector u = vectors.get(j);
				float dist = Math.abs(v.p.x-u.p.x) + Math.abs(v.p.y-u.p.y) + Math.abs(v.p.z-u.p.z);
				if(dist < 5300)
				vds.put(u, new Integer((int) dist));
			}
			distances.put(v,vds);
		}
	}

	public Vector newV(int data) {
		Vector v = new Vector(this,data);
		vid++;
		return v;
	}

	public Edge newE(Vector u, Vector v, long s) {
		Edge e = new Edge(this,u,v);
		e.size = s;
		eid++;
		return e;
	}

	public Edge newE(Vector u, char vdata, long s) {
		return newE(u,newV(vdata),s);
	}

	public void setPos(Vector t, PVector p) {
		posm.put(t, p);
		t.p.set(p);
	}

	public void randomEmptyGraph() {
		int i = randomVertices ;
		while(--i >= 0) {
	          if((i&3)==1)
                      newV((int)aw.random(32)+20);
                  else      
                    newV((int)aw.random(42)+180);
                }
		randomiseVectorPositions();
		loadPos();
	}

	public void randomGraph() {
		int i = randomVertices ;
		int d= 2;
		while(--i >= 0) {
			Vector v = newV((int)(32));
			int dg = d < vectors.size() ? d : vectors.size();
			while(--dg >= 0) 
				newE(v,vectors.get((int)aw.random(vectors.size())),1);
		}
		randomiseVectorPositions();
		loadPos();
	}

	public void loadPos() {
		int i = vectors.size();
		while( --i >= 0 ) {
			Vector v = vectors.get(i);
			vectors.get(i).p.set(posm.get(v));
		}
	}

	public void randomiseVectorPositions() {
		int i = vectors.size();
		while( --i >= 0 ) {
			float x = aw.random(50000)-25000;
                        float z = aw.random((float)Math.sqrt(625000011-x*x));
                        if((i&1)==1)
                          z = -z;
                        float xz = (float)Math.sqrt(625000011-(x*x+z*z));
                        float y = aw.random(xz);
                        if(((int)aw.random(10)&1)==1)
                         y = -y;
                        //float y = -aw.random(20000)-100;
			//float dy = (float) Math.sqrt(10000000000.0-y*y);
			//float z = (float) aw.random(dy)-dy/2;
			//float x = (float) aw.random(dy)-dy/2;
			setPos(vectors.get(i),new PVector(x,y,z));
		}
	}

	public void changePos(Vector t, PVector np) {
		PVector ep = posm.get(t);
		t.p.set(np);
		ep.set(np);
	}

	public Vector closestVector(PVector pos) {
		return null;
	}

	public List<Vector> xSignificantReachable(Vector point) {
		return null;
	}

	public void draw(PVector pos) {
		drawSignificantOnly(xSignificantReachable(closestVector(pos)));
	}

	public void draw() {
		int i;
		switch(type) {
		case(Types.RANDOM_GRAPH):
			i = vectors.size();
		if(aw.verticesOn==1)
			while(--i >= 0)
				vectors.get(i).draw();
		i = edges.size();
		if(aw.edgesOn==1)
			while(--i >= 0)
				edges.get(i).draw();
		break;
		case(Types.RANDOM_EMPTY_GRAPH):
			i = vectors.size();
		if(aw.verticesOn==1)
			while(--i >= 0)
				vectors.get(i).draw();
		break;
		case(Types.LAUNCH_ANIMATION_GRAPH):
			i = vectors.size();
		if(aw.verticesOn==1)
			while(--i >= 0)
				vectors.get(i).draw();
		if(count++ > this.animeFrame) {
			ecolor += 20;
			count = 0;
			if(ecolor > 360)
				ecolor = 0;
		}
		i = edges.size();
                while(--i >= 0) {
			Edge e = edges.get(i);
			if(aw.edgesOn == 1) {
                          if(e.color <= ecolor)
			    	e.draw();
                        }
                        else {
                          if(e.color == ecolor)
			    	e.draw();
                        }
		
                }
		break;
		default:
			break;
		}
	}

	public void update_edge_field() {
		int allVisited = 0;
		while((allVisited=shiftFrontierForward()) != 1) { 
			ecolor += 20;
		}
	}

	public int shiftFrontierForward() {
		int allVisited = 1;
		int multiply = 3;
		int i = frontier.size();
		while(--i >= 0) {
			Vector vf = frontier.get(i);
			int j = multiply;
			while(--j >= 0) {
				Vector successor = closestButNotAlreadyFielded(vf);
				if(successor!=null) {
					allVisited = 0;
					nextFrontier.add(successor);
					fielded.add(successor);
					Edge e = newE(vf,successor,1);
					e.color = ecolor; e.w = 1+360/(ecolor+20);
					e.w = e.w < 10 ? e.w : 10;
				}
			}
		}
		temp = frontier;
		frontier=nextFrontier;
		nextFrontier = temp;
		nextFrontier.clear();
		return allVisited;
	}

	public Vector closestButNotAlreadyFielded(Vector vf) {
		HashMap<Vector,Integer> vfds = distances.get(vf);
		Set<Entry<Vector,Integer>> es = vfds.entrySet();
		Iterator<Entry<Vector,Integer>> esIt = es.iterator();
		int min = 6000000;
		Vector closest = null;
		while(esIt.hasNext()) {
			Entry<Vector,Integer> e = esIt.next();
			if(e.getValue() < min && !fielded.contains(e.getKey())) {
				min = e.getValue();
				closest = e.getKey();
			}
		}
		return closest;
	}

	public void drawSignificantOnly(List<Vector> sigs) {

	}
}
