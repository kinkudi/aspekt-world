import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import processing.core.PVector;


public class Vector {
	public final List<Vector> nv;
	public final List<Edge>ne;
	public final Map<Vector,Edge> nem;
	public final int data;
	public final long id;
	public final Graph g;
	public final PVector p;
	public final int hashcode;
	
	public Vector(Graph gg, int dataa) {
		data = dataa;
                g = gg;
                id = g.vid;
		nv = new ArrayList<Vector>();
		ne = new ArrayList<Edge>();
		nem = new HashMap<Vector,Edge>();
		g.vectors.add(this);
		g.vm.put(this,this);
		hashcode = (int)id;
		p = new PVector(0,0,0);
	}
	
	@Override
	public int hashCode() {
		return hashcode;
	}
	
	@Override
	public boolean equals(Object o) {
		Vector ov = (Vector)o;
		if(ov==this)
			return true;
		if(ov.hashcode != hashcode)
			return false;
		return true;
	}

	public void draw() {
                g.aw.noStroke();
                g.aw.pushMatrix();
		g.aw.fill(data,255,255,200);
		g.aw.translate(p.x,p.y,p.z);
		g.aw.box(200,200,200);
		g.aw.popMatrix();
	}
	
}
