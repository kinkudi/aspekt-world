
public class Edge {
	public final int hashcode;
	public final Vector u, v;
	public long size;
	public final long id;
	public int color;
	public int w;
	public final Graph g;
	
	public Edge (Graph gg, Vector uu, Vector vv) {
		u = uu; v = vv;
		g = gg;
		size = 1;
		id = g.eid;
		u.nv.add(v);
		u.nem.put(v,this);
		u.ne.add(this);
		v.nv.add(u);
		v.nem.put(u,this);
		v.ne.add(this);
		g.edges.add(this);
		hashcode = (int)id;
		color = 200;
	}
	
	@Override
	public int hashCode() {
		return hashcode;
	}
	
	@Override
	public boolean equals(Object o) {
		Edge oe = (Edge)o;
		if(oe==this)
			return true;
		if(oe.hashcode != hashcode)
			return false;
		return true;
	}

	public void draw() {
		g.aw.stroke(color,200,200,200);
		g.aw.strokeWeight(w);
		g.aw.line(u.p.x,u.p.y,u.p.z,v.p.x,v.p.y,v.p.z);
	}
	
}
